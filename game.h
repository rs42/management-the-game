#ifndef __GAME_H__
#define __GAME_H__
#include "stupid_header.h"
#include "clientpool.h"
#include "strings.h"
#include "parse.h"
#include "net.h"
#include "bank.h"

class Game
{
    WrapWL ww;
    typedef enum {start, action, stopped, finish} game_state;
    Bank b;
    ClientPool cp;
    game_state gs;
    int month;
    int ls;
    void cmd_from_stdin();
    void next_month();
public:
    void play();
    Game(int lss, int num_players);
};

#endif
