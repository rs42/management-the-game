#include "stupid_header.h"
#include "game.h"
#include "net.h"

enum {min_ls_port = 4000, max_numof_players = 100};

int main(int argc, char **argv)
{
    signal(SIGPIPE, SIG_IGN);
    int lsport, num_of_players;
    if (argc != 3 ||
        sscanf(argv[1], "%d\n", &lsport) != 1 ||
        lsport < min_ls_port ||
        sscanf(argv[2], "%d\n", &num_of_players) != 1 ||
        num_of_players > max_numof_players ||
        num_of_players < 1)
    {
        printf("GameServer\nUsage: [PORT] [NUMBER OF PLAYERS]\n");
        printf("PORT > %d, NUMBER OF PLAYERS < %d\n",
                min_ls_port, max_numof_players);
        return 0;
    }
    int ls = init_server(lsport);
    Game gg(ls, num_of_players);
    gg.play();
    return 0;
}
