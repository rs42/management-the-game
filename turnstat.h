#ifndef __TURNSTAT_H__
#define __TURNSTAT_H__
typedef enum {getnickname, make_turn, done_turn, bankrupt} client_cond;

enum {firstmonth = -5, lastmonth = 0, working = 1, emptycell = -6,
      endofarray = -10};

struct Turn_par
{
    int rmat_q;
    int rmat_p;
    int gd_q;
    int gd_p;
    int fact_q;
    int prod_q;
};

struct Stat
{
    int money;
    int raw_mat;
    int goods;
    int fact_num;
    int *fact_state;
    int pemp_fact;
};

#endif
