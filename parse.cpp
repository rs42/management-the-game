#include "parse.h"

void free_list(Word_list *start)
{
    Word_list *tmp = start;
    while (start) {
        tmp = tmp->next;
        delete start;
        start = tmp;
    }
}
/*
int strlen2(const char *str)
{
    int i;
    if (str) {
        for (i = 0; str[i] != '\0'; i++)
        {}
        return i + 1;
    } else {
        return 0;
    }
}


char *copy_str(int add_len, char *input, int del_flag)
{
    return new string containing input string plus extra add_len bytes

    if (!input)
        return 0;
    char *output = new char [add_len + strlen2(input)];
    int i;
    for (i = 0; input[i] != '\0'; i++)
        output[i] = input[i];
    output[i] = '\0';
    if (del_flag && input)
        delete [] input;
    return output;
}*/

void WrapWL::refresh(int fd)
{
    if (is_closed) {
        //printf("closed fd\n");
        return;
    }
    if (is_ready) {
        //wl is a string that we dont need anymore
        free_list(wl);
        len = 0;
        p = &wl;
        wl = 0;
        is_ready = 0;
    } else {
        //we should add more words to wl string
    }
    if (is_buffered) {
        //use the buffer instead of read()
    } else {
        stop = read(fd, mainbuf, mainbufsz - 1);
        if (stop < 1) {
            is_closed = 1;
            return;
        }
        start = 0;
        is_ready = 0;
    }
    int i;
    for (i = start; i != stop; i++) {
        if (len > max_len)
            printf("smb tryin to hack server\n");
        if (mainbuf[i] == ' ' || mainbuf[i] == '\t' || mainbuf[i] == '\0')
        {
            if (prev == space || prev == eos) {
                start++;
                continue;
            } else {
                //new word ready until here
                prev = space;
                mainbuf[i] = '\0';
                if (*p) {
                // word from prev buffer needs to be ended
                    (*p)->word = String((*p)->word + mainbuf);
                    p = &((*p)->next);
                } else {
                    //complete word
                    *p = new Word_list;
                    (*p)->word = String(mainbuf + start);
                    (*p)->next = 0;
                    p = &((*p)->next);
                }
                start = i + 1;
            }
        }
        else if (mainbuf[i] == '\n' || mainbuf[i] == '\r')
        {
            if (prev == sym) {
                    //collect last word and exit, too
                    mainbuf[i] = '\0';
                    if (*p) {
                    // word from prev buffer needs to be ended
                        (*p)->word = String((*p)->word + mainbuf);
                        p = &((*p)->next);
                    } else {
                        *p = new Word_list;
                        (*p)->word = String(mainbuf + start);
                        (*p)->next = 0;
                        p = &((*p)->next);
                    }
            }
            //just empty string, exit too
            prev = eos;
            for (; i != stop && (mainbuf[i] == '\n' || mainbuf[i] == '\r'
                || mainbuf[i] == '\0'); i++)
            {}
            start = i;
            break;
        } else if (!(prev == sym)) {
            //non-first word starts here
            start = i;
            prev = sym;
        }
    }
    if (i == stop) {
        if (prev == eos) {
            is_ready = 1;
            //string is ready, even if it's empty
        } else if (prev == sym) {
            is_ready = 0;
            mainbuf[i] = '\0';
            *p = new Word_list;
            (*p)->word = String(mainbuf + start);
            (*p)->next = 0;
            //we need new buffer to finish this word of
            //unfinished string, p is for it
        } else {
            is_ready = 0;
            //we need new buffer to find next word of
            //unfinished string
        }
        is_buffered = 0;
    } else {
        //string is ready buf buffer still has some chars
        is_ready = 1;
        //printf("\nBUFFERED CASE\n");
        is_buffered = 1;
    }
}

//////////////////////////////////////////////////////////
//okie-dockie here
WrapWL::WrapWL()
:wl(0), is_ready(1), is_closed(0), is_buffered(0),
prev(eos), start(0), len(0)
{
    p = &wl;
}

void WrapWL::reset()
{
    wl = 0;
    is_ready =1;
    is_closed = 0;
    is_buffered = 0;
    prev = eos;
    start= 0;
    len = 0;
}

WrapWL::~WrapWL()
{
    free_list(wl);
}

void WrapWL::print() const
{
    for (Word_list *s = wl; s != 0; s = s->next) {
        printf("%s ", s->word.s);
    }
    printf("\n");
}

String WrapWL::operator[](unsigned int i) const
{
    Word_list *t = wl;
    for (unsigned int j = 0; t != 0; j++, t = t->next) {
        if (j == i)
            return t->word;
    }
    throw WordListEmptyError();
}
