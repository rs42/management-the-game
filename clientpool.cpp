#include "clientpool.h"

enum {bufsz = 2048};

void ClientPool::broadcast(const String &s) const
{
    for (int i = 0; i < max_pl; i++) {
        if (pool[i].is_online)
            pool[i].send_msg(s);
    }
}

void ClientPool::add_client(int ls)
{
    if (empty != -1) {
        if ((pool[empty].fd = accept(ls, 0, 0)) == -1) {
            perror("accept");
        } else {
            pool[empty].activate();
            empty = -1;
            for (int i = 0; i != max_pl; i++) {
                if (!pool[i].is_online) {
                    empty = i;
                    break;
                }
            }
        }
    } else {
        refuse_connect(ls);
    }
}

int ClientPool::get_empty() const
{
    return empty;
}

int ClientPool::set_fd(fd_set *readfds) const
{
    int max_d = 0, temp = 0;
    for (int i = 0; i != max_pl; i++) {
        if (pool[i].is_online) {
            temp = pool[i].fd;
            FD_SET(temp, readfds);
            if (temp > max_d)
                max_d = temp;
        }
    }
    return max_d;
}

ClientPool::ClientPool(int num)
:max_pl(num), cur_pl(0), not_bankrupt(0), is_full(0), empty(0)
{
    pool = new Client [num];
    for (int i = 0; i < max_pl; i++) {
        pool[i].my_pool = this;
    }
}

ClientPool::~ClientPool()
{
    delete [] pool;
}

void ClientPool::check_bankrupt()
{
    for (int i = 0; i < max_pl; i++) {
        if (pool[i].cc != bankrupt && pool[i].st.money < 0) {
            pool[i].cc = bankrupt;
            broadcast(pool[i].nickname + " went bankrupt\n");
            not_bankrupt--;
        }
    }
}

void ClientPool::rm_player_from_game(int i)
{
    if (pool[i].st.fact_state)
        delete [] pool[i].st.fact_state;
    pool[i].st.fact_state = 0;
    if (pool[i].is_online) {
        if (pool[i].cc == make_turn || pool[i].cc == done_turn)
            not_bankrupt--;
        if (pool[i].cc != getnickname) {
            char *s = new char [bufsz];
            snprintf(s, bufsz - 1, "Player %s exit\nNow %d/%d\n",
                    pool[i].nickname.s, --cur_pl, max_pl);
            String ss(s);
            delete [] s;
            broadcast(ss);
            printf("%s", ss.s);
            is_full = 0;
        }
        pool[i].is_online = false;
        end_connect(pool[i].fd);
        empty = i;
    }
}

void ClientPool::kick_without_nick()
{
    for (int i = 0; i < max_pl; i++) {
        if (pool[i].cc == getnickname)
            rm_player_from_game(i);
    }
}

bool ClientPool::is_ready() const
{
    for (int i = 0; i < max_pl; i++) {
        if (pool[i].is_online && pool[i].cc == make_turn)
            return false;
    }
    return true;
}

void ClientPool::send_win(int i) const
{
    pool[i].send_msg("WINNER!\n");
}

void ClientPool::send_lose(int i) const
{
    pool[i].send_msg("LOOSER!\n");
}

void ClientPool::congrat() const
{
    for (int i = 0; i < max_pl; i++) {
        if (pool[i].is_online) {
            if (pool[i].cc != bankrupt)
                send_win(i);
            else
                send_lose(i);
        }
    }
}

void ClientPool::handle_before_action(int cl)
{
    if (pool[cl].cc == getnickname) {
        if (pool[cl].set_nick(pool[cl].wp)) {
            String s = String("New player: ") + pool[cl].nickname + "\n";
            broadcast(s);
            printf("%s\n",s.s);
            cur_pl++;
            is_full = (cur_pl == max_pl);
        }
    } else {
        pool[cl].help_msg_before();
    }
}

void ClientPool::treat_action(fd_set *readfds)
{
    for (int i = 0; i < max_pl; i++) {
        if (pool[i].is_online && FD_ISSET(pool[i].fd, readfds)) {
            do {
                pool[i].wp.refresh(pool[i].fd);
                if (pool[i].wp.wl == 0)
                    continue;
                if (pool[i].wp.is_closed) {
                    rm_player_from_game(i);
                } else if (pool[i].wp.is_ready) {
                    #ifdef DEBUG
                    pool[i].wp.print();
                    #endif
                    pool[i].handle_action(pool[i].wp);
                }
            } while (pool[i].wp.is_buffered);
        }
    }
}

void ClientPool::treat_before_action(fd_set *readfds)
{
    for (int i = 0; i < max_pl; i++) {
        if (pool[i].is_online && FD_ISSET(pool[i].fd, readfds)) {
            do {
                pool[i].wp.refresh(pool[i].fd);
                if (pool[i].wp.wl == 0)
                    continue;
                if (pool[i].wp.is_closed) {
                    rm_player_from_game(i);
                } else if (pool[i].wp.is_ready) {
                    #ifdef DEBUG
                    pool[i].wp.print();
                    #endif
                    handle_before_action(i);
                }
            } while (pool[i].wp.is_buffered);
        }
    }
}

void ClientPool::init_banks(Bank *b)
{
    for (int i = 0; i < max_pl; i++) {
        pool[i].my_bank = b;
    }
}

void ClientPool::print_clients_data_long(Client &c)
{/*
    char *s = new char [bufsz];
    for (int i = 0, j = 0; i != max_pl; i++) {
        if (pool[i].is_online &&
            pool[i].cc != bankrupt &&
            pool[i].cc != getnickname)
        {
            j++;
            snprintf(s, bufsz -1, "%d: %s\n", j, pool[i].nickname.s);
            c.send_msg(s);
            snprintf(s, bufsz -1, "MONEY: %d\n", pool[i].st.money);
            c.send_msg(s);
            snprintf(s, bufsz -1, "MATERIALS: %d\n", pool[i].st.raw_mat);
            c.send_msg(s);
            snprintf(s, bufsz -1, "GOODS: %d\n", pool[i].st.goods);
            c.send_msg(s);
            snprintf(s, bufsz -1, "WORKING FACTORIES: %d\n",
                    pool[i].st.fact_num);
            c.send_msg(s);
        }
    }
    delete [] s;*/
    for (int i = 0, j = 0; i != max_pl; i++) {
        if (pool[i].is_online && pool[i].cc != bankrupt &&
            pool[i].cc != getnickname)
        {
            j++;
            dprintf(c.fd, "%d: %s\n", j, pool[i].nickname.s);
            dprintf(c.fd, "MONEY: %d\n", pool[i].st.money);
            dprintf(c.fd, "MATERIALS: %d\n", pool[i].st.raw_mat);
            dprintf(c.fd, "GOODS: %d\n", pool[i].st.goods);
            dprintf(c.fd, "WORKING FACTORIES: %d\n", pool[i].st.fact_num);
        }
    }
}

void ClientPool::print_clients_data_short()
{
    printf("STATUS CODES:\n");
    printf("GETNICK 0 MAKETURN 1 DONETURN 2 BANKRUPT 3\n");
    for (int i = 0, j = 0; i != max_pl; i++) {
        if (pool[i].is_online) {
            j++;
            if (pool[i].cc != getnickname)
                printf("%d: %s %d\n", j, pool[i].nickname.s, pool[i].cc);
            else
                printf("%d: NONAME %d\n", j, pool[i].cc);
        }
    }
}

void ClientPool::map_produce_goods()
{
    for (int i = 0; i < max_pl; i++)
        pool[i].s_produce_goods();
    check_bankrupt();
}

void ClientPool::map_build_factories()
{
    for (int i = 0; i < max_pl; i++)
        pool[i].s_build_factories();
    check_bankrupt();
}

void ClientPool::map_costs_before_trade()
{
    for (int i = 0; i < max_pl; i++) {
        if (pool[i].is_online && pool[i].cc != bankrupt)
            pool[i].s_costs_before_trade();
    }
    check_bankrupt();
}

void ClientPool::map_costs_after_trade()
{
    for (int i = 0; i < max_pl; i++) {
        if (pool[i].is_online && pool[i].cc != bankrupt)
            pool[i].s_costs_after_trade();
    }
    check_bankrupt();
}

void ClientPool::map_goods_produced()
{
    for (int i = 0; i < max_pl; i++) {
        if (pool[i].is_online)
            pool[i].s_goods_produced();
    }
}

void ClientPool::map_new_turn()
{
    for (int i = 0; i < max_pl; i++) {
        pool[i].s_new_turn();
    }
}


void ClientPool::send_stats_to_bots(int month)
{
    for (int i = 0; i < max_pl; i++) {
        if (pool[i].is_online && pool[i].is_bot) {
            int d = pool[i].fd;
            int stt = (pool[i].cc == bankrupt) ? 0 : 1;
            Bank *b = pool[i].my_bank;
            dprintf(d, "# %d %d %d %d %d ", month, stt, i, not_bankrupt,
                max_pl);
            dprintf(d, "%d %d %d %d ", b->market_descr[0],
                b->market_descr[1], b->market_descr[2], b->market_descr[3]);
            dprintf(d, "%d %d %d %d %d ", m_raw_mat, m_goods, m_factory,
                u_factory, u_prod);
            #ifdef DEBUG
            printf("# %d %d %d %d %d %d %d %d %d %d %d %d %d %d\n",
              month, stt, i, not_bankrupt, max_pl, b->market_descr[0],
            b->market_descr[1], b->market_descr[2], b->market_descr[3],
                m_raw_mat, m_goods, m_factory, u_factory, u_prod);
            #endif
            for (int j = 0; j != max_pl; j++) {
                 if (pool[j].is_online && pool[j].cc != bankrupt) {
                    dprintf(d, "# %d ", j);
                    dprintf(d, "%d ", pool[j].st.money);
                    dprintf(d, "%d ", pool[j].st.raw_mat);
                    dprintf(d, "%d ", pool[j].st.goods);
                    dprintf(d, "%d ", pool[j].st.fact_num);
                    dprintf(d, "%d ", pool[j].tp.prod_q);
                    dprintf(d, "%d ", pool[j].sold_goods_q);
                    dprintf(d, "%d ", pool[j].sold_goods_p);
                    dprintf(d, "%d ", pool[j].bou_rw_q);
                    dprintf(d, "%d ", pool[j].bou_rw_p);
                }
                #ifdef DEBUG
                printf("# %d %d %d %d %d %d %d %d %d %d\n", j,
                  pool[j].st.money,pool[j].st.raw_mat,
                  pool[j].st.goods,pool[j].st.fact_num,pool[j].tp.prod_q,
                  pool[j].sold_goods_q,pool[j].sold_goods_p,
                  pool[j].bou_rw_q,pool[j].bou_rw_p);
                #endif
            }
            dprintf(d, "\n");
        }
    }
}
/*
void ClientPool::send_stats_to_bots(int month)
{
    char ss[bufsz];
    for (int i = 0; i < max_pl; i++) {
        if (pool[i].is_online && pool[i].is_bot) {
            int d = pool[i].fd;
            int stt = (pool[i].cc == bankrupt) ? 0 : 1;
            Bank *b = pool[i].my_bank;
            snprintf(ss, bufsz, "# %d %d %d %d %d ", month, stt, i, not_bankrupt,
                max_pl);
            String(ss).send(d);
            snprintf(ss, bufsz, "%d %d %d %d ", b->market_descr[0],
                b->market_descr[1], b->market_descr[2], b->market_descr[3]);
            String(ss).send(d);
            for (int j = 0; j != max_pl; j++) {
                 if (pool[j].is_online && pool[j].cc != bankrupt) {
                    snprintf(ss, bufsz, "# %d ", j);
                    String(ss).send(d);
                    snprintf(ss, bufsz, "%d ", pool[j].st.money);
                    String(ss).send(d);
                    snprintf(ss, bufsz, "%d ", pool[j].st.raw_mat);
                    String(ss).send(d);
                    snprintf(ss, bufsz, "%d ", pool[j].st.goods);
                    String(ss).send(d);
                    snprintf(ss, bufsz, "%d ", pool[j].st.fact_num);
                    String(ss).send(d);
                    snprintf(ss, bufsz, "%d ", pool[j].tp.prod_q);
                    String(ss).send(d);
                    snprintf(ss, bufsz, "%d ", pool[j].sold_goods_q);
                    String(ss).send(d);
                    snprintf(ss, bufsz, "%d ", pool[j].sold_goods_p);
                    String(ss).send(d);
                    snprintf(ss, bufsz, "%d ", pool[j].bou_rw_q);
                    String(ss).send(d);
                    snprintf(ss, bufsz, "%d ", pool[j].bou_rw_p);
                    String(ss).send(d);
                }
            }
            String(" \n").send(d);
        }
    }
}*/
