#ifndef __PARSE_H__
#define __PARSE_H__
#include "stupid_header.h"
#include "strings.h"
class WordListEmptyError
{};


struct Word_list
{
    String word;
    Word_list *next;
};

struct WrapWL
{
    enum {mainbufsz = 2049, max_len = 100};
    typedef enum {space, eos, sym} prev_st;
    Word_list *wl;
    bool is_ready;
    bool is_closed;
    bool is_buffered;
    char mainbuf[mainbufsz];
    prev_st prev;
    int start, stop;
    Word_list **p;
    int len;
    WrapWL();
    ~WrapWL();
    void print() const;
    void reset();
    String operator[](unsigned int i) const;
    void refresh(int fd);
};
#endif
