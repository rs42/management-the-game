#include "net.h"

int init_server(int lsport)
{
    struct sockaddr_in addr;
    int ls;
    if ((ls = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }
    addr.sin_family = AF_INET;
    addr.sin_port = htons(lsport);
    addr.sin_addr.s_addr = INADDR_ANY;
    int opt = 1;
    setsockopt(ls, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

    if (bind(ls, (struct sockaddr *) &addr, sizeof(addr))) {
        perror("bind");
        exit(1);
    }
    if (listen(ls, 5)) {
        perror("listen");
        exit(1);
    }
    return ls;
}

void end_connect(int fd)
{
    String s("Connection is being closed now.\n");
    if (fd > 0) {
        s.send(fd);
        shutdown(fd, 2);
        close(fd);
    }
}

void refuse_connect(int ls)
{
    String s("Access denied.\n");
    int fd = accept(ls, NULL, NULL);
    if (fd != -1) {
        s.send(fd);
        end_connect(fd);
    }
}
