#ifndef __CLIENT_H__
#define __CLIENT_H__
#include "turnstat.h"
#include "net.h"
#include "strings.h"
#include "parse.h"
struct Bank;
struct ClientPool;

struct Client
{
    bool is_online;
    bool is_bot;
    client_cond cc;
    int fd;
    String nickname;
    Stat st;
    Netinfo ni;
    Turn_par tp;
    Bank *my_bank;
    ClientPool *my_pool;
    WrapWL wp;

    int sold_goods_q;
    int sold_goods_p;
    int bou_rw_q;
    int bou_rw_p;
    Client();
    ~Client();
    void activate();
    void send_msg(const String &s) const;
    void help_msg_before() const;
    void help_msg_during() const;
    void welcome_msg() const;
    void nickname_error() const;
    bool set_nick(const WrapWL &ww);
    void handle_action(WrapWL &w);
    void print_dec() const;
    bool sok(WrapWL &ww);
    bool sprod(WrapWL &ww);
    bool sbuild(WrapWL &ww);
    bool ssell(WrapWL &ww);
    bool sbuy(WrapWL &ww);
    bool sdec(WrapWL &ww);
    bool sstat(WrapWL &ww);
    bool smarket(WrapWL &ww);

    int *enlarge_fact_array(int *p);

    void s_produce_goods();
    void s_build_factories();
    void s_costs_before_trade();
    void s_costs_after_trade();
    void s_goods_produced();
    void s_new_turn();
};
#endif
