#include "bank.h"
#include "client.h"
#include "clientpool.h"

enum {df_level = 2};
enum {bufsz = 4096};

const int level_change[5][5] = {
    {4, 4, 2, 1, 1 },
    {3, 4, 3, 1, 1 },
    {1, 3, 4, 3, 1 },
    {1, 1, 3, 4, 3 },
    {1, 1, 2, 4, 4 }
};


const int market_levels[5][4] = {
    {2, 800, 6, 6500},
    {3, 650, 5, 6000},
    {4, 500, 4, 5500},
    {5, 400, 3, 5000},
    {6, 300, 2, 4500}
}; /*1 and 3rd collumns must be divided by 2*/
/*raw_materials (q, p) and goods (q, p)*/
enum {rw_ip = 1, gd_ip = 3};

Bank::Bank(int players)
:trade_level(df_level)
{
    srand(time(0));
    calc_market_state(players);
}

void Bank::calc_market_state(int players)
{
    int cl = trade_level;
    market_descr[0] = (market_levels[cl][0] * players) / 2;
    market_descr[1] = market_levels[cl][1];
    market_descr[2] = (market_levels[cl][2] * players) / 2;
    market_descr[3] = market_levels[cl][3];
}

static int rand_from1to_n(int n)
{
    return 1 + (int)((double)n * rand() / (RAND_MAX + 1.0));
}

void Bank::change_market_state(int players)
{
    int r = rand_from1to_n(12);
    int i, s;
    for (i = 0, s = 0;; i++) {
        s+=level_change[trade_level][i];
        if (s >= r)
            break;
    }
    trade_level = i;
    calc_market_state(players);
}


void Bank::print_market_state(Client &c)
{
    char *s = new char [bufsz];
    snprintf(s, bufsz - 1, "***MARKET STATE***\n");
    c.send_msg(s);
    snprintf(s, bufsz - 1,"LEVEL: %d\n", trade_level);
    c.send_msg(s);
    snprintf(s, bufsz - 1,
        "BUY RAW MATERIALS: %d available starting from $%d\n",
        market_descr[0], market_descr[1]);
    c.send_msg(s);
    snprintf(s, bufsz - 1,
        "SELL GOODS: %d available with highest bid price $%d\n",
        market_descr[2], market_descr[3]);
    c.send_msg(s);
    snprintf(s, bufsz - 1, "***ENDOFSECTION***\n");
    c.send_msg(s);
    delete [] s;
}

const char
*action_denied = "Bank: transaction denied.\n",
*action_accepted = "Bank: transaction accepted.\n",
*n_en_money = "Bank: you'll run out of money. Make another decision.\n";

bool can_afford(int m, Turn_par tp)
{
    return m >= tp.fact_q * u_factory +
                tp.prod_q * u_prod + tp.rmat_p * tp.rmat_q;
}

void Bank::check_buy(int q, int p, Client &c)
{
    Turn_par tp = c.tp;
    tp.rmat_q = q;
    tp.rmat_p = p;
    if (p < market_descr[rw_ip] || q > market_descr[0] || q <= 0) {
        c.tp.rmat_p = 0;
        c.tp.rmat_q = 0;
        c.send_msg(action_denied);
    } else if (!can_afford(c.st.money, tp)) {
        c.send_msg(n_en_money);
    } else {
        c.tp.rmat_p = p;
        c.tp.rmat_q = q;
        c.send_msg(action_accepted);
    }
}

void Bank::check_sell(int q, int p, Client &c)
{
    if (p <= 0 || q <= 0 || p > market_descr[gd_ip] ||
        q > c.st.goods || q > market_descr[2])
    {
        c.tp.gd_p = 0;
        c.tp.gd_q = 0;
        c.send_msg(action_denied);
    } else {
        c.tp.gd_q = q;
        c.tp.gd_p = p;
        c.send_msg(action_accepted);
    }
}

void Bank::check_build(int q, Client &c)
{
    Turn_par tp = c.tp;
    tp.fact_q = q;
    if (q < 0) {
        c.send_msg(action_denied);
    } else if (!can_afford(c.st.money, tp)) {
        c.send_msg(n_en_money);
    } else {
        c.tp.fact_q = q;
        c.send_msg(action_accepted);
    }
}

void Bank::check_prod(int q, Client &c)
{
    Turn_par tp = c.tp;
    tp.prod_q = q;
    if (q < 0 || q > c.st.fact_num || q > c.st.raw_mat) {
        c.send_msg(action_denied);
    } else if (!can_afford(c.st.money, tp)) {
        c.send_msg(n_en_money);
    } else {
        c.tp.prod_q = q;
        c.send_msg(action_accepted);
    }
}

void Bank::produce_goods(Client &c)
{
    if (c.is_online && c.cc == done_turn) {
        c.st.money -= u_prod * c.tp.prod_q;
        c.st.raw_mat -= c.tp.prod_q;
    }
}

void Bank::build_factories(Client &c)
{
    if (c.is_online && c.cc == done_turn) {
        for (int j = 0; j < c.tp.fact_q; j++) {
            if (c.st.fact_state[c.st.pemp_fact] == endofarray)
                c.st.fact_state = c.enlarge_fact_array(c.st.fact_state);
            c.st.fact_state[c.st.pemp_fact++] = firstmonth;
        }
    }
}

void Bank::costs_before_trade(Client &c)
{
    c.st.money -= m_factory * c.st.fact_num;
    c.st.money -= m_raw_mat * c.st.raw_mat;
    int new_fact = 0;
    for (int j = 0; c.st.fact_state[j] != endofarray; j++) {
        if (c.st.fact_state[j] == firstmonth)
            c.st.money -= u_factory;
        if (c.st.fact_state[j] == lastmonth) {
            c.st.money -= u_factory;
            new_fact++;
            c.st.fact_num++;
        }
        if (c.st.fact_state[j] >= firstmonth &&
            c.st.fact_state[j] <= lastmonth)
        {
            c.st.fact_state[j]++;
        }
    }
    if (new_fact) {
        char *s = new char [bufsz];
        snprintf(s, bufsz - 1,
            "%d new factories are ready next month", new_fact);
        c.send_msg(s);
        delete [] s;
        //dprintf(c.fd, "%d new factories are ready next month", new_fact);
    }
}

struct Client_data_list
{
    int i_client;
    int q;
    Client_data_list *next;
};

struct Same_bets_list
{
    int bet;
    int n_comp;
    Client_data_list *cll;
    Same_bets_list *next;
};

void free_cl_data_list(Client_data_list *p)
{
    Client_data_list *s = p;
    while (p != 0) {
        p = p->next;
        delete s;
        s = p;
    }
}

void free_bets_list(Same_bets_list *p)
{
    Same_bets_list *s = p;
    while (p != 0) {
        p = p->next;
        free_cl_data_list(s->cll);
        delete s;
        s = p;
    }
}


bool des(int a, int b)
{
    return a < b;
}

bool asc(int a, int b)
{
    return a > b;
}

int add_2_list(Same_bets_list **pp,
                int i_client,
                int bet,
                int quantity,
                bool (*cmp)(int a, int b))
{
/*des for sell raw materials*/
/*asc for buying goods*/
    Same_bets_list *p = *pp;
    if (p == 0) {
    /*new list*/
        *pp = new Same_bets_list;
        (*pp)->bet = bet;
        (*pp)->n_comp = 1;
        (*pp)->next = 0;
        (*pp)->cll = new Client_data_list;
        (*pp)->cll->i_client = i_client;
        (*pp)->cll->next = 0;
        (*pp)->cll->q = quantity;
        return 0;
    }
    if (cmp(p->bet, bet)) {
        /*new first node*/
        p = new Same_bets_list;
        p->bet = bet;
        p->n_comp = 1;
        p->next = *pp;
        p->cll = new Client_data_list;
        p->cll->i_client = i_client;
        p->cll->next = 0;
        p->cll->q = quantity;
        *pp = p;
        return 0;
    }
    for (;; p = p->next) {
        if (p->bet == bet) {
            /*just add new i_client to this node*/
            p->n_comp++;
            Client_data_list *tmp = new Client_data_list;
            tmp->i_client = i_client;
            tmp->next = p->cll;
            tmp->q = quantity;
            p->cll = tmp;
            return 0;
        }
        if (p->next == 0) {
            /*add new node to the end of the list*/
            p->next = new Same_bets_list;
            p = p->next;
            p->next = 0;
            p->bet = bet;
            p->n_comp = 1;
            p->cll = new Client_data_list;
            p->cll->next = 0;
            p->cll->q = quantity;
            p->cll->i_client = i_client;
            return 0;
        }
        if (cmp(p->next->bet, bet)) {
            /*add between*/
            Same_bets_list *tmp = new Same_bets_list;
            tmp->next = p->next;
            p->next = tmp;
            tmp->bet = bet;
            tmp->n_comp = 1;
            tmp->cll = new Client_data_list;
            tmp->cll->next = 0;
            tmp->cll->i_client = i_client;
            tmp->cll->q = quantity;
            return 0;
        }
    }
}

typedef enum {trade_rm, trade_goods} trade_type;

void
bets_analyzer(ClientPool &c, int units, Same_bets_list *st, trade_type tt)
{
    Same_bets_list *tmp;
    int i, r , total;
    char *s = new char [bufsz];
    Client_data_list *pcd;
    for (tmp = st; units > 0 && tmp;) {
        if ((pcd = tmp->cll)) {
            r = rand_from1to_n(tmp->n_comp--);
            for (i = 1; i < r; i++)
                pcd = pcd->next;
            if (pcd->q > units)
                total = units;
            else
                total = pcd->q;
            units-=total;
            if (tt == trade_rm) {
                c.pool[pcd->i_client].st.raw_mat += total;
                c.pool[pcd->i_client].st.money -= total * tmp->bet;
                snprintf(s, bufsz - 1, "%s got %d rawmat units for $%d\n",
                    c.pool[pcd->i_client].nickname.s, total, tmp->bet);
                c.pool[pcd->i_client].bou_rw_q = total;
                c.pool[pcd->i_client].bou_rw_p = tmp->bet;
            } else {
                c.pool[pcd->i_client].st.goods -= total;
                c.pool[pcd->i_client].st.money += total * tmp->bet;
            	snprintf(s, bufsz - 1, "%s sold %d goods units for $%d\n",
                    c.pool[pcd->i_client].nickname.s, total, tmp->bet);
                c.pool[pcd->i_client].sold_goods_q = total;
                c.pool[pcd->i_client].sold_goods_p = tmp->bet;
            }
            c.broadcast(s);
            /*delete pcd from list*/
            Client_data_list *foo = tmp->cll;
            if (foo == pcd) {
                tmp->cll = tmp->cll->next;
                delete foo;
            } else {
                for (;foo->next != pcd; foo = foo->next)
                {}
                foo->next = pcd->next;
                delete pcd;
            }
        } else {
            tmp = tmp->next;
        }
    }
    delete [] s;
}

void Bank::trades(ClientPool &p)
{
    int goodss = market_descr[2];
    int raw_mat = market_descr[0];
    Same_bets_list *rm_sbl = 0, *gd_sbl = 0;
    for (int i = 0; i < p.max_pl; i++) {
        if (p.pool[i].is_online && p.pool[i].cc == done_turn) {
            if (p.pool[i].tp.gd_p > 0 && p.pool[i].tp.gd_q > 0) {
                add_2_list(&gd_sbl, i, p.pool[i].tp.gd_p,
                            p.pool[i].tp.gd_q, asc);
            }
            if (p.pool[i].tp.rmat_p > 0 && p.pool[i].tp.rmat_q > 0) {
                add_2_list(&rm_sbl, i, p.pool[i].tp.rmat_p,
                            p.pool[i].tp.rmat_q, des);
            }
        }
    }
    bets_analyzer(p, goodss, gd_sbl, trade_goods);
    bets_analyzer(p, raw_mat, rm_sbl, trade_rm);
    free_bets_list(rm_sbl);
    free_bets_list(gd_sbl);
    p.check_bankrupt();
}

void Bank::costs_after_trade(Client &c)
{
    c.st.money -= m_goods * c.st.goods;
}
