#include "game.h"

enum {bufsz = 4096};


Game::Game(int lss, int num_players)
:b(0), cp(num_players), gs(start), month(0), ls(lss)
{}

void Game::cmd_from_stdin()
{
    do {
        ww.refresh(0);
        if (ww.is_closed) {
            gs = finish;
            cp.broadcast("SERVER SHUTS DOWN, GAME FINISHED\n");
        } else if (ww.is_ready && ww.wl) {
            ww.print();
            if (ww[0] == "start" && gs == start) {
                gs = action;
            } else {
                cp.print_clients_data_short();
                printf("ONLINE: %d\nMAX: %d\n", cp.cur_pl, cp.max_pl);
            }
        }
    } while (ww.is_buffered);
}

void Game::next_month()
{
    month++;
    b.change_market_state(cp.not_bankrupt);
}

void Game::play()
{
    int should_senttobots = 1;
    int max_d;
    fd_set readfds;
    while (gs == start) {
        if (cp.is_full)
            break;
        FD_ZERO(&readfds);
        FD_SET(ls, &readfds);
        FD_SET(0, &readfds);
        max_d = cp.set_fd(&readfds);
        if (ls > max_d)
            max_d = ls;
        if (select(max_d + 1, &readfds, 0, 0, 0) < 1) {
            perror("select");
            continue;
        }
        if (FD_ISSET(ls, &readfds))
            cp.add_client(ls);
        if (FD_ISSET(0, &readfds))
            cmd_from_stdin();
        cp.treat_before_action(&readfds);
    }
    cp.broadcast("GAME STARTS NOW\n");
    gs = action;
    cp.kick_without_nick();
    if (cp.cur_pl < 2)
        goto end;
    b = Bank(cp.cur_pl);
    cp.not_bankrupt = cp.cur_pl;
    cp.init_banks(&b);
    while (gs == action) {
        if (should_senttobots) {
            cp.send_stats_to_bots(month);
            cp.map_new_turn();
            should_senttobots = 0;
        }
        FD_ZERO(&readfds);
        FD_SET(ls, &readfds);
        FD_SET(0, &readfds);
        max_d = cp.set_fd(&readfds);
        if (ls > max_d)
            max_d = ls;
        if (select(max_d + 1, &readfds, 0, 0, 0) < 1) {
            perror("select");
            continue;
        }
        if (FD_ISSET(ls, &readfds))
            refuse_connect(ls);
        if (FD_ISSET(0, &readfds))
            cmd_from_stdin();
        cp.treat_action(&readfds);
        if (cp.is_ready()) {
            /*trades and taxes and costs*/
            cp.map_produce_goods();
            cp.map_build_factories();
            cp.map_costs_before_trade();
            b.trades(cp);
            cp.map_costs_after_trade();
            cp.map_goods_produced();
            if (cp.not_bankrupt < 2 || cp.cur_pl < 2)
                break;
            next_month();
            should_senttobots = 1;
        }
    }
    end:
    cp.congrat();
}
