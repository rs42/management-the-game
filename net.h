#ifndef __NET_H__
#define __NET_H__
#include "stupid_header.h"
#include "strings.h"
struct Netinfo
{
    struct sockaddr_in addr;
    int addrlen;
};
int init_server(int lsport);
void end_connect(int fd);
void refuse_connect(int ls);
#endif
