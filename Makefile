CC = g++
OBJECTS = main.o bank.o client.o clientpool.o game.o strings.o net.o parse.o
DFFLAGS = -g -Wall -c
ALLDEPS = turnstat.h stupid_header.h main.cpp bank.cpp bank.h client.cpp client.h clientpool.cpp clientpool.h game.cpp game.h strings.cpp strings.h net.cpp net.h parse.cpp parse.h


server: $(OBJECTS)
	$(CC) -g -o $@ $(OBJECTS)

.cpp.o: $(ALLDEPS)
	$(CC) $(DFFLAGS) $<

clean:
	rm -f $(OBJECTS) server
