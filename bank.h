#ifndef __BANK_H__
#define __BANK_H__
#include "stupid_header.h"
#include "strings.h"
#include "time.h"
typedef enum {m_raw_mat = 300, m_goods = 500, m_factory = 1000} month_costs;

typedef enum {u_factory = 2500, u_prod = 2000} unit_costs;
struct Client;
struct ClientPool;

struct Bank
{
    int trade_level;
    int market_descr[4];

    void calc_market_state(int players);
    void change_market_state(int players);
    Bank(int players);

    void print_market_state(Client &c);
    void check_buy(int q, int p, Client &c);
    void check_sell(int q, int p, Client &c);
    void check_build(int q, Client &c);
    void check_prod(int q, Client &c);

    void produce_goods(Client &c);
    void build_factories(Client &c);
    void costs_before_trade(Client &c);
    void costs_after_trade(Client &c);
    void trades(ClientPool &pool);
};
#endif
