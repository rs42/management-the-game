#ifndef __CLIENTPOOL_H__
#define __CLIENTPOOL_H__
#include "client.h"
#include "parse.h"
#include "bank.h"
#include "strings.h"

struct ClientPool
{
    Client *pool;
    int max_pl, cur_pl, not_bankrupt;
    bool is_full;
    int empty; //index of non-online client, -1 if no place

    ClientPool(int num);
    ~ClientPool();
    int set_fd(fd_set *readfds) const;
    int get_empty() const;
    void add_client(int ls);
    void broadcast(const String &s) const;
    void check_bankrupt();
    void rm_player_from_game(int i);
    void kick_without_nick();
    bool is_ready() const;
    void congrat() const;
    void send_win(int i) const;
    void send_lose(int i) const;
    void handle_before_action(int cl);
    void treat_action(fd_set *readfds);
    void treat_before_action(fd_set *readfds);
    void init_banks(Bank *b);
    void send_stats_to_bots(int month);


    void print_clients_data_long(Client &c);
    void print_clients_data_short();
    void map_produce_goods();
    void map_build_factories();
    void map_costs_before_trade();
    void map_costs_after_trade();
    void map_goods_produced();
    void map_new_turn();
};
#endif
