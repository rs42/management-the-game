#include "client.h"
#include "clientpool.h"
#include "bank.h"

enum {bufsz = 4096, max_nick = 10, min_nick = 2};

enum
{
    df_money = 10000,
    df_raw = 4,
    df_goods = 2,
    df_factories = 2,
    add_factory = 10
};

const Turn_par df_turn = {0, 0, 0, 0, 0, 0};

const Stat df_stat =
{df_money, df_raw, df_goods, df_factories, 0, df_factories};

Client::Client()
: is_online(false), is_bot(0), cc(getnickname), nickname(0), st(df_stat),
  tp(df_turn)
{
    sold_goods_p = sold_goods_q = bou_rw_q = bou_rw_p = 0;
}

void Client::send_msg(const String &s) const
{
    s.send(fd);
}

Client::~Client()
{
    if (st.fact_state)
        delete [] st.fact_state;
}

const String enter_nick("Enter your nickname. Try to think out unique one\n");

void Client::activate()
{
    st.fact_state = new int [df_factories + add_factory + 1];
    int i;
    for (i = 0; i < df_factories; i++)
        st.fact_state[i] = working;
    for (; i < df_factories + add_factory; i++)
        st.fact_state[i] = emptycell;
    st.fact_state[i] = endofarray;
    is_online = true;
    cc = getnickname;
    wp.reset();
    enter_nick.send(fd);
}


void Client::help_msg_before() const
{
    send_msg(
    String("This is a manual of Management Game. Before the game start\n") +
        "(you'll get a message when it'll happen) you can learn some basics:\n"
    );
    help_msg_during();

}

void Client::help_msg_during() const
{
    send_msg(
    String(
        "market - display all prices and other stuff\n") +
        "stat - display players table\n" +
        "dec - display your decision during current turn\n" +
        "buy [PRICE] [QUANTITY] - buy raw materials\n" +
        "sell [PRICE] [QUANTITY] - sell your goods\n" +
        "build [QUANTITY] - build num of factories\n" +
        "prod [QUANTITY] - product new goods\n" +
        "ok - confirm your turn, then you can't change your decision\n" +
        "All values are empty by default, this man page is always available\n" +
        "when you send an unpropriate request\n"
    );
}

void Client::welcome_msg() const
{
    send_msg("Welcome!\n");
}

void Client::nickname_error() const
{
    char *s = new char [bufsz];
    snprintf(s, bufsz - 1, "Nickname must be > %d and < %d\n",
            min_nick, max_nick);
    send_msg(s);
    delete [] s;
    //dprintf(fd, "Nickname must be > %d and < %d\n", min_nick, max_nick);
}


bool Client::set_nick(const WrapWL &ww)
{
    if (!ww.is_closed && ww.is_ready &&
        ww[0].sz > min_nick && ww[0].sz <= max_nick)
    {
        if (ww[0] == "##") {
            nickname = ww[1];
            is_bot = 1;
        } else {
            nickname = ww[0];
        }
        cc = make_turn;
        return true;
    }
    nickname_error();
    return false;
}


bool Client::smarket(WrapWL &ww)
{
    if (ww[0] == "market") {
        my_bank->print_market_state(*this);
        return true;
    } else {
        return false;
    }
}

bool Client::sstat(WrapWL &ww)
{
    if (ww[0] == "stat") {
        my_pool->print_clients_data_long(*this);
        return true;
    } else {
        return false;
    }
}

void Client::print_dec() const
{
    if (cc != bankrupt) {
        char *s = new char [bufsz];
        snprintf(s, bufsz - 1, "Factories to build: %d\n", tp.fact_q);
        send_msg(s);
        snprintf(s, bufsz - 1, "Goods to produce: %d\n", tp.prod_q);
        send_msg(s);
        snprintf(s, bufsz - 1, "Sell %d goods, price %d $/unit\n",
                tp.gd_q, tp.gd_p);
        send_msg(s);
        snprintf(s, bufsz -1, "Buy %d raw_mat, price %d $/unit\n",
            tp.rmat_q, tp.rmat_p);
        send_msg(s);
        delete [] s;
        /*dprintf(fd, "Factories to build: %d\n", tp.fact_q);
        dprintf(fd, "Goods to produce: %d\n", tp.prod_q);
        dprintf(fd, "Sell %d goods, price %d $/unit\n", tp.gd_q, tp.gd_p);
        dprintf(fd, "Buy %d raw_mat, price %d $/unit\n", tp.rmat_q, tp.rmat_p);
        */
    }
}

bool Client::sdec(WrapWL &ww)
{
    if (ww[0] ==  "dec") {
        if (cc != make_turn) {
            send_msg("Relax\n");
            return true;
        }
        print_dec();
        return true;
    } else {
        return false;
    }
}

bool Client::sbuy(WrapWL &ww)
{
    int q, p;
    if (ww[0] == "buy" && sscanf(ww[1].s, "%d\n", &q) &&
        sscanf(ww[2].s, "%d\n", &p))
    {
        if (cc != make_turn) {
            send_msg(nickname + ", are you tryin' to hack me?\n");
            return true;
        }
        my_bank->check_buy(q, p, *this);
        return true;
    } else {
        return false;
    }
}

bool Client::ssell(WrapWL &ww)
{
    int p, q;
    if (ww[0] == "sell" && sscanf(ww[1].s, "%d\n", &q) &&
        sscanf(ww[2].s, "%d\n", &p))
    {
        if (cc != make_turn) {
            send_msg("Don't bother me\n");
            return true;
        }
        my_bank->check_sell(q, p, *this);
        return true;
    } else {
        return false;
    }
}

bool Client::sbuild(WrapWL &ww)
{
    int q;
    if (ww[0] == "build" && sscanf(ww[1].s, "%d\n", &q)) {
        if (cc != make_turn) {
            send_msg("I hate you\n");
            return true;
        }
        my_bank->check_build(q, *this);
        return true;
    } else {
        return false;
    }
}

bool Client::sprod(WrapWL &ww)
{
    int q;
    if (ww[0] == "prod" && sscanf(ww[1].s, "%d\n", &q)) {
        if (cc != make_turn) {
            send_msg("When I grow up I'll be stable...\n");
            return true;
        }
        my_bank->check_prod(q, *this);
        return true;
    } else {
        return false;
    }
}

bool Client::sok(WrapWL &ww)
{
    if (ww[0] ==  "ok") {
        if (cc != make_turn) {
            send_msg("Keep calm\n");
            return true;
        }
        cc = done_turn;
        return true;
    } else {
        return false;
    }
}


void Client::handle_action(WrapWL &w)
{
    if (sbuild(w)) {}
    else if (sbuy(w)) {}
    else if (sdec(w)) {}
    else if (smarket(w)) {}
    else if (sok(w)) {}
    else if (sprod(w)) {}
    else if (ssell(w)) {}
    else if (sstat(w)) {}
    else {
        help_msg_during();
    }
}


void Client::s_produce_goods()
{
    my_bank->produce_goods(*this);
}

void Client::s_build_factories()
{
    my_bank->build_factories(*this);
}

int *Client::enlarge_fact_array(int *p)
{
    int old_len;
    for (old_len = 0; p[old_len] != endofarray; old_len++)
    {}
    int *new_arr = new int [old_len + add_factory + 1];
    int i;
    for (i = 0; p[i] != endofarray; i++)
        new_arr[i] = p[i];
    for (; i < old_len + add_factory; i++)
        new_arr[i] = emptycell;
    new_arr[i] = endofarray;
    delete [] p;
    return new_arr;
}

void Client::s_costs_before_trade()
{
    my_bank->costs_before_trade(*this);
}

void Client::s_costs_after_trade()
{
    my_bank->costs_after_trade(*this);
}

void Client::s_goods_produced()
{
    if (is_online && cc == done_turn)
        st.goods += tp.prod_q;
}

void Client::s_new_turn()
{
    if (is_online && cc == done_turn) {
        cc = make_turn;
        tp = df_turn;
        sold_goods_p = sold_goods_q = bou_rw_q = bou_rw_p = 0;
    }
}
